const { ClientApplication } = require('./client')

let userClient = new ClientApplication();

const transientData = {
    make: Buffer.from("House"),
    model: Buffer.from("Terraced"),
    color: Buffer.from("Calicut"),
    brokerName: Buffer.from("Broker 2")
}

userClient.submitTxn(
    "broker",
    "realestatechannel",
    "XYZ-Realestate",
    "OrderContract",
    "privateTxn",
    transientData,
    "createOrder",
    "Order-05",
).then(result => {
    console.log(new TextDecoder().decode(result))
})