const { ClientApplication } = require('./client');

let userClient = new ClientApplication();
userClient.submitTxn(
    "buyer",
    "realestatechannel",
    "XYZ-Realestate",
    "PropertyContract",
    "invokeTxn",
    "",
    "createProperty",
    "Property-05",
    "House",
    "Terraced",
    "Calicut",
    "22/05/2023",
    "Buyer-2"
).then(result => {
    console.log(new TextDecoder().decode(result))
})