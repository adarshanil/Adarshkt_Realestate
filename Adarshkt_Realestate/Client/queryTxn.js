const { ClientApplication } = require('./client')

let userClient = new ClientApplication()
userClient.submitTxn(
    "buyer",
    "user1",
    "realestatechannel",
    "XYZ-Realestate",
    "PropertyContract",
    "queryTxn",
    "",
    "readProperty",
    "Property-05",
).then(result => {
    console.log(new TextDecoder().decode(result))
});

userClient.submitTxn(
    "broker",
    "realestatechannel",
    "XYZ-Realestate",
    "OrderContract",
    "queryTxn",
    "",
    "readOrder",
    "Order-05",
).then(result => {
    console.log(new TextDecoder().decode(result))
});

