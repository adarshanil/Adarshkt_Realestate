let profile={
    buyer:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/buyer.realestate.com",
        "CP":"../Network/vars/profiles/realestatechannel_connection_for_nodesdk.json"
    },
    broker:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/broker.realestate.com",
        "CP":"../Network/vars/profiles/realestatechannel_connection_for_nodesdk.json"
    },
    seller:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/seller.realestate.com",
        "CP":"../Network/vars/profiles/realestatechannel_connection_for_nodesdk.json"
    }

}
module.exports={profile}

