let profile = {
    buyer: {
        "cryptoPath": "../Network/vars/keyfiles/peerOrganizations/buyer.realestate.com",
        "keyDirectoryPath": "../Network/vars/keyfiles/peerOrganizations/buyer.realestate.com/users/Admin@buyer.realestate.com/msp/keystore",
        "certPath": "../Network/vars/keyfiles/peerOrganizations/buyer.realestate.com/users/Admin@buyer.realestate.com/msp/signcerts/Admin@buyer.realestate.com-cert.pem",
        "tlsCertPath": "../Network/vars/keyfiles/peerOrganizations/buyer.realestate.com/users/Admin@buyer.realestate.com/tls/ca.crt",
        "peerEndpoint": "localhost:7003",
        "peerHostAlias": "peer1.buyer.realestate.com",
        "mspId": "buyer-realestate-com"


    },
    broker: {
        "cryptoPath": "../Network/vars/keyfiles/peerOrganizations/broker.realestate.com",
        "keyDirectoryPath": "../Network/vars/keyfiles/peerOrganizations/broker.realestate.com/users/Admin@broker.realestate.com/msp/keystore",
        "certPath": "../Network/vars/keyfiles/peerOrganizations/broker.realestate.com/users/Admin@broker.realestate.com/msp/signcerts/Admin@broker.realestate.com-cert.pem",
        "tlsCertPath": "../Network/vars/keyfiles/peerOrganizations/broker.realestate.com/users/Admin@broker.realestate.com/tls/ca.crt",
        "peerEndpoint": "localhost:7004",
        "peerHostAlias": "peer1.broker.realestate.com",
        "mspId": "broker-realestate-com"
    },
    seller: {
        "cryptoPath": "../Network/vars/keyfiles/peerOrganizations/seller.realestate.com",
        "keyDirectoryPath": "../Network/vars/keyfiles/peerOrganizations/seller.realestate.com/users/Admin@seller.realestate.com/msp/keystore",
        "certPath": "../Network/vars/keyfiles/peerOrganizations/seller.realestate.com/users/Admin@seller.realestate.com/msp/signcerts/Admin@seller.realestate.com-cert.pem",
        "tlsCertPath": "../Network/vars/keyfiles/peerOrganizations/seller.realestate.com/users/Admin@seller.realestate.com/tls/ca.crt",
        "peerEndpoint": "localhost:7005",
        "peerHostAlias": "peer1.seller.realestate.com",
        "mspId": "seller-realestate-com"
    }
}
module.exports = { profile }
