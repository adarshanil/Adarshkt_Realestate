/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const OrderContract = require('./order-contract');

class PropertyContract extends Contract {

    async propertyExists(ctx, propertyId) {
        const buffer = await ctx.stub.getState(propertyId);
        return (!!buffer && buffer.length > 0);
    }

    async createProperty(ctx, propertyId, make, model, place, dateOfBuy, buyerName) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'buyer-realestate-com') {
            const exists = await this.propertyExists(ctx, propertyId);
            if (exists) {
                throw new Error(`The property ${propertyId} already exists`);
            }
            const asset = {
                make,
                model,
                place,
                dateOfBuy,
                status: 'In Transaction',
                ownedBy: buyerName,
                assetType: 'property'
            };
            const buffer = Buffer.from(JSON.stringify(asset));
            await ctx.stub.putState(propertyId, buffer);
        }
        else {
            return `User under the following MSP: ${mspID} cannot perform this action`;
        }

    }

    async readProperty(ctx, propertyId) {
        const exists = await this.propertyExists(ctx, propertyId);
        if (!exists) {
            throw new Error(`The property ${propertyId} does not exist`);
        }
        const buffer = await ctx.stub.getState(propertyId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }


    async deleteProperty(ctx, propertyId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'buyer-realestate-com') {
            const exists = await this.propertyExists(ctx, propertyId);
            if (!exists) {
                throw new Error(`The property ${propertyId} does not exist`);
            }
            await ctx.stub.deleteState(propertyId);
        }
        else {
            return `User under the following MSP: ${mspID} cannot perform this action`;
        }

    }


    async queryAllPropertys(ctx) {
        const queryString = {
            selector: {
                assetType: 'property'
            },
            sort: [{ place: 'asc' }]
        };

        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
        let result = await this._getAllResults(resultIterator);
        return JSON.stringify(result)

    }

    async getPropertysByRange(ctx, startKey, endKey) {
        let resultIterator = await ctx.stub.getStateByRange(startKey, endKey);
        let result = await this._getAllResults(resultIterator);
        return JSON.stringify(result)
    }

    async getPropertyHistory(ctx, propertyId) {
        let resultIterator = await ctx.stub.getHistoryForKey(propertyId)
        let result = await this._getAllResults(resultIterator, true);
        return JSON.stringify(result)
    }

    async _getAllResults(iterator, isHistory) {
        let allResult = [];

        let res = await iterator.next();
        while (!res.done) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {};
                if (isHistory && isHistory === true) {
                    jsonRes.TxId = res.value.txId
                    jsonRes.Timestamp = res.value.timestamp
                    jsonRes.Record = JSON.parse(res.value.value.toString())
                }
                else {
                    jsonRes.Key = res.value.key;
                    jsonRes.Record = JSON.parse(res.value.value.toString());
                }
                allResult.push(jsonRes)
            }
            res = await iterator.next()
        }
        await iterator.close()
        return allResult
    }


    async getPropertysWithPagination(ctx, pageSize, bookMark) {
        const queryString = {
            selector: {
                assetType: 'property'
            }
        }
        const pageSizeInt = parseInt(pageSize, 10)
        const { iterator, metadata } = await ctx.stub.getQueryResultWithPagination(JSON.stringify(queryString), pageSizeInt, bookMark)
        let result = await this._getAllResults(iterator, false)
        let results = {};
        results.Result = result
        console.log('metadata%%%%%%%%%%%%%%%%%', metadata);
        results.ResponseMetaData = {
            RecordCount: metadata.fetchedRecordsCount,
            Bookmark: metadata.bookmark
        }
        return JSON.stringify(results)
    }

    async checkMatchingOrders(ctx, propertyId) {
        const exists = await this.propertyExists(ctx, propertyId);
        if (!exists) {
            throw new Error(`The property ${propertyId} does not exist`);
        }

        const propertyBuffer = await ctx.stub.getState(propertyId);
        const propertyDetails = JSON.parse(propertyBuffer.toString());

        const queryString = {
            selector: {
                assetType: 'order',
                make: propertyDetails.make,
                model: propertyDetails.model,
                place: propertyDetails.place,
            },
        };

        const orderContract = new OrderContract();
        const orders = await orderContract._queryAllOrdersWithQueryString(
            ctx,
            queryString
        );

        return orders;
    }

    async matchOrder(ctx, propertyId, orderId) {
        const orderContract = new OrderContract();

        const propertyexists = await this.propertyExists(ctx, propertyId);
        if (!propertyexists) {
            throw new Error(`The property ${propertyId} does not exist`);
        }
        const orderexists = await orderContract.orderExists(ctx, orderId);
        if (!orderexists) {
            throw new Error(`The order ${orderId} does not exist`);
        }
        const propertyDetails = await this.readProperty(ctx, propertyId);
        const orderDetails = await orderContract.readOrder(ctx, orderId);

        if (
            orderDetails.make === propertyDetails.make &&
            orderDetails.model === propertyDetails.model &&
            orderDetails.place === propertyDetails.place
        ) {
            propertyDetails.ownedBy = orderDetails.brokerName;
            propertyDetails.status = 'Assigned to a Broker';

            const newPropertyBuffer = Buffer.from(JSON.stringify(propertyDetails));
            await ctx.stub.putState(propertyId, newPropertyBuffer);

            await orderContract.deleteOrder(ctx, orderId);
            return `Property ${propertyId} is assigned to ${orderDetails.brokerName}`;
        } else {
            return 'Order is not matching';
        }
    }
    
    async registerProperty(ctx, propertyId, ownerName, registrationNumber) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'seller-realestate-com') {
            const exists = await this.propertyExists(ctx, propertyId);
            if (!exists) {
                throw new Error(`The property ${propertyId} does not exist`);
            }

            const propertyBuffer = await ctx.stub.getState(propertyId);
            const propertyDetails = JSON.parse(propertyBuffer.toString());

            propertyDetails.status = `Registered to ${ownerName} wih plate number ${registrationNumber}`;
            propertyDetails.ownedBy = ownerName;

            const newPropertyBuffer = Buffer.from(JSON.stringify(propertyDetails));
            await ctx.stub.putState(propertyId, newPropertyBuffer);

            return `Property ${propertyId} is successfully registered to ${ownerName}`;
        } else {
            return `User under following MSP:${mspID} cannot able to perform this action`;
        }
    }

}

module.exports = PropertyContract;
