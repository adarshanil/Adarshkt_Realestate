/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const PropertyContract = require('./lib/property-contract');
const OrderContract = require('./lib/order-contract');


module.exports.PropertyContract = PropertyContract;
module.exports.OrderContract = OrderContract;

module.exports.contracts = [ PropertyContract, OrderContract ];
