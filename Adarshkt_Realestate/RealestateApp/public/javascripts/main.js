const addData = async(event) => {

  event.preventDefault();

  const propertyId = document.getElementById("propertyId").value;
  const make = document.getElementById("make").value;
  const model = document.getElementById("model").value;
  const place = document.getElementById("place").value;
  const dateOfBuy = document.getElementById("dateOfBuy").value;
  const buyerName = document.getElementById("buyerName").value;

  console.log(propertyId);

  if (
    propertyId.length == 0 ||
    make.length == 0 ||
    model.length == 0 ||
    place.length == 0 ||
    dateOfBuy.length == 0 ||
    buyerName.length == 0
  ) {
    alert("Please enter the data properly");
  } else {
    
    try{
        const response = await fetch("/api/property", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              propertyId,
              make,
              model,
              place,
              dateOfBuy,
              buyerName,
            }),
          });
    
          console.log(response)
    
          return alert("Property Created");

    }catch(err){
        alert("Error");
        console.log(err);
    }

  }

};

const readData = async (event) => {
    event.preventDefault();
    const propertyId = document.getElementById("propertyIdInput").value;
    //console.log(propertyId)
  
    if (propertyId.length == 0) {
      alert("Please enter the data properly");
    } else {
      try {
        const response = await fetch(`/api/property/${propertyId}`);
  
        let responseData = await response.json();
  
        console.log(responseData);
  
        alert(JSON.stringify(responseData));
  
      } catch (err) {
        alert("Error");
        console.log(err);
      }
    }
  };