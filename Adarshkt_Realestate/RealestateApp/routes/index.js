var express = require("express");
var router = express.Router();
const { ClientApplication } = require("../../Client/client");
let userClient = new ClientApplication();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Buyer Dashboard" });
});

router.post("/api/property", (req, res) => {
  userClient.submitTxn(
    "buyer",
    "realestatechannel",
    "XYZ-Realestate",
    "PropertyContract",
    "invokeTxn",
    "",
    "createProperty",
    `${req.body.propertyId}`,
    `${req.body.make}`,
    `${req.body.model}`,
    `${req.body.place}`,
    `${req.body.dateOfBuy}`,
    `${req.body.buyerName}`
  ).then((message) => {
    // console.log(message.toString())
    console.log(new TextDecoder().decode(message));
    res.send("Property Created");
  });
});

router.get("/api/property/:propertyId", (req, res) => {
  userClient.submitTxn(
    "buyer",
    "realestatechannel",
    "XYZ-Realestate",
    "PropertyContract",
    "queryTxn",
    "",
    "readProperty",
    `${req.params.propertyId}`
  ).then((message) => {
    // console.log(message.toString())
    console.log(new TextDecoder().decode(message));
    res.send(new TextDecoder().decode(message));
  });

  //res.send("get request");
});


module.exports = router;
