#!/bin/sh

echo "Start the network"
minifab netup -s couchdb -e true -i 2.4.8 -o buyer.realestate.com

sleep 5

echo "Create the channel"
minifab create -c realestatechannel

sleep 2

echo "Join the peers to channel"
minifab join -c realestatechannel

sleep 2

echo "Anchor peer update"
minifab anchorupdate

sleep 2

echo "Generate the profiles"
minifab profilegen -c realestatechannel


