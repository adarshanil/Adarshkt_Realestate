#!/bin/bash

minifab cleanup

sudo rm -r vars

docker volume rm $(docker volume ls -q)

docker rm $(docker container ls -q) --force

yes | docker container prune

yes | docker system prune

yes | docker volume prune

yes | docker network prune

echo ""
docker ps -a